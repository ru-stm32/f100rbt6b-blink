#!/bin/bash

readonly NAME=f100rbt6b-blink

main() {

    if [ -z $ARM_TOOLCHAIN ]; then
        echo "Enviroment variable ARM_TOOLCHAIN not set"
        exit 1
    fi

    openocd -f "./openocd.cfg" -c "program ./target/thumbv7m-none-eabi/release/$NAME verify reset exit"

    # $ARM_TOOLCHAIN/arm-none-eabi-objcopy -v -O ihex "./target/thumbv7m-none-eabi/release/$NAME" "./target/$NAME.hex" || exit 2
    # openocd -f "./openocd.cfg" -c "program ./target/$NAME.hex verify reset exit"

    # $ARM_TOOLCHAIN/arm-none-eabi-objcopy -v -O binary "./target/thumbv7m-none-eabi/release/$NAME" "./target/$NAME.bin" || exit 2
    # st-flash write "./target/$NAME.bin" 0x08000000
}

main
