# f100rbt6b-blink

Blink in Rust for STM32VLDISCOVERY development board

## dependencies

```sh
rustup target add thumbv6m-none-eabi thumbv7m-none-eabi thumbv7em-none-eabi thumbv7em-none-eabihf
```

## ARM toolchain

```sh
export ARM_TOOLCHAIN="/path/to/gcc-arm-none-eabi/bin"
```

## Disassembly

```sh
$ARM_TOOLCHAIN/arm-none-eabi-objdump --disassemble target/thumbv7m-none-eabi/release/f100rbt6b-blink | less
```

## Convert _.elf_

```sh
# bin
$ARM_TOOLCHAIN/arm-none-eabi-objcopy -O binary target/thumbv7m-none-eabi/release/f100rbt6b-blink f100rbt6b-blink.bin
# hex
$ARM_TOOLCHAIN/arm-none-eabi-objcopy -O ihex target/thumbv7m-none-eabi/release/f100rbt6b-blink f100rbt6b-blink.hex
```
